## ADD NFS STORAGE

### 1. Создать namespace

kubectl create ns monitoring

### 2. Helm для NFS хранилки

helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/  
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \  
--namespace monitoring \  
--set nfs.server=10.2.0.26 \  
--set nfs.path=/NFS \  
--set storageClass.name=nfs  

### 3. Создаем StorageClass & PVC

kubectl create -f monitoring-config.yaml -n monitoring  

## PROMETHEUS + GRAFANA

### [ ГАЙДИК ](https://docs.appsealing.com/guide/4.%20On-Premise/7.%20Logging_and_monitoring_with_Prometheus_Grafana_Loki.html#what-is-this-content-based-on)

helm install prom prometheus-community/kube-prometheus-stack -n monitoring --values values.yaml  
helm repo add grafana https://grafana.github.io/helm-charts  
helm repo update  

## LOKI FOR YC S3 BUCKET

helm upgrade --install loki grafana/loki-distributed -n monitoring --set service.type=LoadBalancer \  
  --namespace monitoring \  
  --set loki-distributed.loki.storageConfig.aws.bucketnames=s3-diploma \  
  --set loki-distributed.serviceaccountawskeyvalue_generated.accessKeyID=YCAJElcrcZmR81BE2cxMLGd8B \  
  --set loki-distributed.serviceaccountawskeyvalue_generated.secretAccessKey=YCPbUi-ndV_DjW-Zeln3YCs0dTuvZkQ5RI4OW2a9  
helm upgrade --install promtail grafana/promtail -f values.yaml -n monitoring  
 
